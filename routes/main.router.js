/*
Imports
 */
// NodeJS
const { Router } = require('express');
// Inner
const AuthRouterClass = require('./api/auth/auth.routes');
const ChatRouterClass = require('./api/chat/chat.routes');
const FrontChatRouterClass = require('./front/chat/front.chat.routes');
//

/*
Define routers
 */

// Global
const mainRouter = Router({ mergeParams: true });
const apiRouter = Router({ mergeParams: true });
const frontRouter = Router({ mergeParams: true });

// Childs
// API
const authRouter = new AuthRouterClass();
const chatRouter = new ChatRouterClass();
// FRONT
const frontChatRouter = new FrontChatRouterClass();

//

/*
Define routes
 */
mainRouter.use('/api/', apiRouter);
mainRouter.use('/', frontRouter);

// API
apiRouter.use('/auth', authRouter.init());
apiRouter.use('/chat', chatRouter.init());

// FRONT
frontRouter.use('/chat', frontChatRouter.init());
//

/*
Exports
 */
module.exports = { mainRouter };
//