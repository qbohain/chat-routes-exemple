/*
Imports
 */
// Node
const fetch = require('node-fetch');
//

/*
Functions
 */
// Read stored messages
const readMessages = () => {

    return new Promise( (resolve, reject) => {

        fetch('http://localhost:4000/api/chat/messages')
            .then(res => res.json())
            .then(json => resolve(json))
            .catch(error => reject(error));

    });

};
//

/*
Exports
 */
module.exports = { readMessages };
//