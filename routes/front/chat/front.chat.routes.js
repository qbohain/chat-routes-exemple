/*
Imports
 */
// NodeJS
const express = require('express');
const frontChatRouter = express.Router({ mergeParams: true });
// Inner
const { readMessages } = require('./front.chat.controller');
//

/*
Routes definition
 */
class FrontChatRouterClass {

    routes() {
        // Messages list
        frontChatRouter.get('/', (req, res) => {

            // Read all messages
            readMessages()
                .then(response => {
                    res.render('chat', {
                        messagesList: response.data
                    })
                })
                .catch(error => error);

        });
    }

    init() {
        this.routes();
        return frontChatRouter;
    }

}

/*
Exports
 */
module.exports = FrontChatRouterClass;
//