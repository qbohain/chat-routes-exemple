/*
Imports
 */
// NodeJS
const express = require('express');
const chatRouter = express.Router({ mergeParams: true });
// Inner
const { sendMessage, loadMessagesList, deleteMessage } = require('./chat.controller');
const { checkFields } = require('../../../services/request.checker');
const { sendBodyError, sendFieldsError, sendApiSuccessResponse, sendApiErrorResponse } = require('../../../services/server.response');
//

/*
Routes definition
 */
class ChatRouterClass {

    routes() {

        // HATEOAS
        chatRouter.get('/', (req, res) => {
            res.json('HATEOAS for chat');
        });

        // Load messages list
        chatRouter.get('/messages', (req, res) => {
            loadMessagesList()
                .then( apiResponse => sendApiSuccessResponse(res, 'Messages loaded', apiResponse) )
                .catch( apiResponse => sendApiErrorResponse(res, 'Loading messages failed', apiResponse) )
        });

        // Send message
        chatRouter.post('/message', (req, res) => {

            // Vérifier la présence du body
            if( typeof req.body === undefined || req.body === null)
                sendBodyError( res, 'No body data provided' );

            // Vérifier les champs du body
            const { ok, extra, miss } = checkFields( ['message', 'user'], req.body );

            // Vérifier si les champs du body sont valides
            if(!ok)
                sendFieldsError( res, 'Bad fields provided', miss, extra );
            else
                sendMessage(req.body)
                    .then( apiResponse => sendApiSuccessResponse(res, 'Message sent', apiResponse) )
                    .catch( apiResponse => sendApiErrorResponse(res, 'Send message failed', apiResponse) )

        });

        // Delete message
        chatRouter.delete('/message', (req, res) => {
            // Vérifier la présence du body
            if( typeof req.body === undefined || req.body === null)
                sendBodyError( res, 'No body data provided' );

            // Vérifier les champs du body
            const { ok, extra, miss } = checkFields( ['id', 'user'], req.body );

            // Vérifier si les champs du body sont valides
            if(!ok)
                sendFieldsError( res, 'Bad fields provided', miss, extra );
            else
                deleteMessage(req.body)
                    .then( apiResponse => sendApiSuccessResponse(res, 'Message deleted', apiResponse) )
                    .catch( apiResponse => sendApiErrorResponse(res, 'Message delete failed', apiResponse) )
        });

    }

    init() {
        this.routes();
        return chatRouter;
    }

}
//

/*
Exports
 */
module.exports = ChatRouterClass;
//