/*
Imports & configs
 */
const mongoose = require('mongoose');
const { Schema } = mongoose;
//

/*
Model definition
 */
const chatSchema = new Schema({
    message: String,
    user: String,
    date: { type: Date, default: Date.now }
});
//

/*
Exports
 */
const ChatModel = mongoose.model('chat', chatSchema);
module.exports = ChatModel;
//