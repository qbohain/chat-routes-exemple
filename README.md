# Chat routes exemple

Evaluation NodeJS / MongoDB

Exemple de routes et controlleur pour un chat :
- Créer un message chat
- Supprimer un message (utilisateur)
- Charger les nouveaux messages

## Setup
Renommer .env-exemple par .env, et remplissez les champs avec vos valeurs.

## Lancer l'app :
npm start
