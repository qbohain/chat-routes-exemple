/*
Créer une fonction qui vérifie pour vérifier les données d'une requête
 */
const checkFields = ( required, reqBody ) => {

    // Création du tableau pour les champs manquants ou en trop
    let miss = [];
    let extra = [];

    // Vérifier qu'il ne manque pas de champs
    required.forEach( prop => {
        if( !(prop in reqBody) )
            miss.push(prop);
    });

    // Vérifier les champs en trop
    for (let prop in reqBody) {
        if( required.indexOf(prop) === -1 )
            extra.push(prop);
    };

    // Vérifier les champs
    const ok = ( extra.length === 0 && miss.length === 0);

    // Renvoyer le résultat
    return { ok, extra, miss };

};
//

/*
Exporter le module du service
 */
module.exports = { checkFields };
//