/*
Imports
 */
// NodeJS
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
require('dotenv').config(); // Configurer l'utilisation des variables d'environnement
// Inner
const { mainRouter } = require('./routes/main.router')
const db = require('./services/db');
//

/*
Server configuration
 */
const port = process.env.PORT;
let chatServer = express();
//

/*
Server init
 */
const init = () => {

    //=> MongoDB
    db.initClient();

    //=> NodeJS
    chatServer.set( 'view engine', 'ejs' );
    chatServer.set( 'views', path.join(__dirname, 'www/views') );
    chatServer.use( express.static( path.join(__dirname, 'www') ) );
    chatServer.use( bodyParser.json({ limit: '10mb' }) );
    chatServer.use( bodyParser.urlencoded({ extended: true }) );

    //=> Router
    chatServer.use('/', mainRouter);

    //=> Server launch
    chatServer.listen(port, () => {
        console.log(`Server is running on port ${port}`);
    });

}
//

// Start server
init();